class AddIncidentDateToTickets < ActiveRecord::Migration
  def change
    add_column :tickets, :incident_date, :date
  end
end
