class AddDefendingClubToTickets < ActiveRecord::Migration
  def change
    add_column :tickets, :defending_club, :integer
  end
end
