class RemoveDefendingAgainAgainClubFromTickets < ActiveRecord::Migration
  def change
    remove_column :tickets, :defending_club, :integer
  end
end
