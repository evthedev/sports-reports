class AddDefendingClubAgainToTickets < ActiveRecord::Migration
  def change
    add_column :tickets, :defending_club_id, :integer
  end
end
