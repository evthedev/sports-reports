class AddIncidentNameToTickets < ActiveRecord::Migration
  def change
    add_column :tickets, :incident_name, :string
  end
end
