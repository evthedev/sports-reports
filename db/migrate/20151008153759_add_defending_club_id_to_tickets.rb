class AddDefendingClubIdToTickets < ActiveRecord::Migration
  def change
    add_column :tickets, :defending_club_id, :integer, foreign_key: true
  end
end
