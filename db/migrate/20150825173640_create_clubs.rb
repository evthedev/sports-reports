class CreateClubs < ActiveRecord::Migration
  def change
    create_table :clubs do |t|
      t.string :name
      t.references :club_pres, foreign_key: true
      t.references :assoc_pres, foreign_key: true

      t.timestamps null: false
    end
  end
end
