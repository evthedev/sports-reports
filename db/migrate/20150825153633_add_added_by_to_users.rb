class AddAddedByToUsers < ActiveRecord::Migration
  def change
    add_column :users, :added_by, :integer
  end
end
