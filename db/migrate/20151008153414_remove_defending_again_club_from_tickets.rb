class RemoveDefendingAgainClubFromTickets < ActiveRecord::Migration
  def change
    remove_column :tickets, :defending_club_id, :integer
  end
end
