class RemoveDefendingClubFromTickets < ActiveRecord::Migration
  def change
  	remove_column :tickets, :defending_club
  end
end
