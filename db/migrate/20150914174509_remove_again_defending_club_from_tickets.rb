class RemoveAgainDefendingClubFromTickets < ActiveRecord::Migration
  def change
  	remove_column :tickets, :defending_club
  end
end
