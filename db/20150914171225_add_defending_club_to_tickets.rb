class AddDefendingClubToTickets < ActiveRecord::Migration
  def change
    add_column :tickets, :defending_club, :integer, foreign_key: true
  end
end
