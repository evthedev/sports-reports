class Club < ActiveRecord::Base
  belongs_to :club_pres, class_name: 'User'
  belongs_to :assoc_pres, class_name: 'User'
  has_many :tickets
  validates :name, uniqueness: true

end


