class ClubsController < ApplicationController
	
  load_and_authorize_resource :club 
  skip_authorization_check only: :create
  skip_before_action :verify_authenticity_token, only: :create, if: 'request.format.json?'

  def index
    @clubs = Club.paginate(page: params[:page])
  end

  def new
    @club = Club.new
  end

  def edit
    @club = Club.find(params[:id])
  end

  def create
    @club = Club.new(club_params)

    if @club.save
      redirect_to clubs_path, notice: I18n::translate(:club_added)
    else
      render 'new'
    end

  end
  def update
    if @club.update_attributes(club_params)
      redirect_to clubs_path, notice: t(:club_modified)
    else
      render 'edit'
    end
  end

  private
  	
	def club_params
	  params.require(:club).permit(
	  	:name,
	  	:assoc_pres_id,
	  	:club_pres_id
  	)
	end


end
